<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use JWTAuth;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::query()->where('email', $request->input('email'))->first();

        if (! $user) {
            return response()->json(['success' => false, 'message' => 'Invalid User'], 400);
        }
        
        if (! $token = JWTAuth::claims(['user' => $user])->attempt(
            ['email' => $request->input('email'), 'password' => $request->input('password')]
        )) {
            return response()->json(['success' => false, 'message' => 'Invalid User'], 400);
        }

        return response()->json([
            'succees' => true,
            'message' => 'Login successfull',
            'token' => $token
        ]);
    }

    public function logout()
    {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }
}
