<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateProductRequest;
use App\Models\Product;

class ProductController extends Controller
{
    public function createProduct(CreateProductRequest $request)
    {
        Product::create([
            'name' => $request->name,
            'description' => $request->description
        ]);

        return response()->json([
            'succees' => true,
            'message' => 'product created successfully'
        ]);
    }
}
