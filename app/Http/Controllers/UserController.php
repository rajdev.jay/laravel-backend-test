<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\CreateUserRequest;
class UserController extends Controller
{
    public function createUser(CreateUserRequest $request)
    {
        $user = auth()->user();
        if ($user->roll_id == 1) {
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'roll_id' => 0
            ]);

            return response()->json([
                'succees' => true,
                'message' => 'user created successfully'
            ]);
        }

        return response()->json([
            'succees' => false,
            'message' => 'You do not have permission to create user'
        ]);
    }
}
