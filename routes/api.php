<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login','App\Http\Controllers\LoginController@login');


Route::middleware([App\Http\Middleware\AuthMiddleware::class])->group(function () {
    Route::post('create/user','App\Http\Controllers\UserController@createUser');

    Route::post('create/post','App\Http\Controllers\ProductController@createProduct');

    Route::post('logout','App\Http\Controllers\LoginController@logout');

});